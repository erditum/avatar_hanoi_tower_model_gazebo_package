#include <stdio.h>
#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainiksolvervel_pinv_givens.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <Eigen/Dense>
using namespace std;



void linspace(KDL::Vector vec1, KDL::Vector vec2, size_t N , std::vector<KDL::Vector> &trajectory) {

   

    double h_0 = (vec2[0] - vec1[0]) / static_cast<double>(N-1);
    double h_1 = (vec2[1] - vec1[1]) / static_cast<double>(N-1);
    double h_2 = (vec2[2] - vec1[2]) / static_cast<double>(N-1);
    KDL::Vector xs;
    for (int i = 0 ; i<N ; i++){
        xs[0] = vec1[0] + h_0 * i ; 
        xs[1] = vec1[1] + h_1 * i ; 
        xs[2] = vec1[2] + h_2 * i ; 
        trajectory.push_back(xs);
    }
}



void SimulationDelay(double second){
      // Get current time and delay the simulation.
      auto end = std::chrono::system_clock::now();
      std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    //   std::cout << "Current Time Now:  " << std::ctime(&end_time);
      usleep(second * 1000000 ); // 1 second = 1e6 micro seconds

    }



KDL::Frame  Transformation_matrix_hips_to_world_frame(){
    // This transformation matrix based on the location hips to the world frame
    KDL::Vector pos_hips_to_world = KDL::Vector(-0.45832319676974104, 0.09999866419115455,0); //dont change
    KDL::Rotation rot_hips_to_world;
    rot_hips_to_world = rot_hips_to_world.Quaternion(0,0,0.7071068, 0.7071068);
    // rot_target = rot_target.Quaternion(0,0,0,1);
    KDL::Frame T_hips_to_world(rot_hips_to_world, pos_hips_to_world);

    return T_hips_to_world;

}

KDL::Frame go_target_position(KDL::Vector pos, KDL::Rotation rot){
    

    KDL::Frame T_goal(rot, pos);
    T_goal = Transformation_matrix_hips_to_world_frame().Inverse() * T_goal;

    return T_goal;




}


KDL::JntArray  inverse_kinematic_solver(KDL::Chain kuka_arm_chain,int number_of_joints, KDL::Frame T_goal){

    // Takes goal frame return the joint angles

    // Inverse kinematic solution test
    double eps = 1E-5;
    int maxiter = 1500;
    double eps_joints = 1E-15;
    KDL::ChainIkSolverPos_LMA iksolver = KDL::ChainIkSolverPos_LMA(kuka_arm_chain, eps, maxiter, eps_joints);


    KDL::JntArray jointpositions = KDL::JntArray(number_of_joints);
    KDL::JntArray jointGuesspositions = KDL::JntArray(number_of_joints);

    bool kinematics_status;
    KDL::JntArray q(number_of_joints); // used for calculation forward kinematics
    kinematics_status = iksolver.CartToJnt(jointGuesspositions, T_goal, jointpositions);
    if (kinematics_status >= 0)
    {
        for (int i = 0; i < number_of_joints; i++)
        {
            std::cout << jointpositions(i) << std::endl;
            q(i) = jointpositions(i);
        }
        printf("%s \n", "Success, thanks KDL!");
    }
    else
    {
        printf("%s \n", "Error:could not calculate backword kinematics : ");
    }
    return jointpositions;
}

KDL::Frame forward_kinematic_solver(KDL::Chain kuka_arm_chain, KDL::JntArray target_joint_pos){

    KDL::ChainFkSolverPos_recursive fksolver(kuka_arm_chain);
    KDL::Frame T;
    fksolver.JntToCart(target_joint_pos, T);
    return T;

}

void pick_and_place_token(ros::NodeHandle &node, KDL::Chain &kuka_arm_chain, std::vector<KDL::Vector> &trajectory_points,std::string token_name){
    node.setParam("/counter_reset", true);
    int number_of_joints = kuka_arm_chain.getNrOfJoints();
    std::vector<double> mixamorig_RightArm_z;
    std::vector<double> mixamorig_RightArm_x;
    std::vector<double> mixamorig_RightArm_y;
    std::vector<double> mixamorig_RightForeArm;
    std::vector<double> mixamorig_RightHand_z;
    std::vector<double> mixamorig_RightHand_x;
    std::vector<double> mixamorig_RightHand_y;
    for (int i = 0; i<trajectory_points.size() ; i++) {
    // KDL::Vector pos_target = KDL::Vector(0.1,-0.24-0.005*i,1.17);
    KDL::Rotation rot_target;
    rot_target = rot_target.Quaternion(0,0,1,0);
    KDL::Frame T_goal = go_target_position(trajectory_points[i],rot_target);

    Eigen::MatrixXd Goal_Frame_Matrix(4, 4);
    Goal_Frame_Matrix << T_goal(0, 0), T_goal(0, 1), T_goal(0, 2), T_goal(0, 3),
                         T_goal(1, 0), T_goal(1, 1), T_goal(1, 2), T_goal(1, 3),
                         T_goal(2, 0), T_goal(2, 1), T_goal(2, 2), T_goal(2, 3),
                         T_goal(3, 0), T_goal(3, 1), T_goal(3, 2), T_goal(3, 3);

    std::cout << "Goal Transformation Matrix: " << std::endl;
    std::cout << Goal_Frame_Matrix << std::endl;


    KDL::JntArray target_joint_pos  =  inverse_kinematic_solver(kuka_arm_chain,number_of_joints,T_goal);


    std::cout << target_joint_pos.rows() << std::endl;


    mixamorig_RightArm_z.push_back( target_joint_pos(0));
    mixamorig_RightArm_x.push_back( target_joint_pos(1));
    mixamorig_RightArm_y.push_back( target_joint_pos(2));
    mixamorig_RightForeArm.push_back( target_joint_pos(3));
    mixamorig_RightHand_z.push_back( target_joint_pos(4));
    mixamorig_RightHand_x.push_back( target_joint_pos(5));
    mixamorig_RightHand_y.push_back( target_joint_pos(6));



    std::cout << "Verify the solution with Forward Kinematics" << std::endl;

    KDL::Frame T = forward_kinematic_solver(kuka_arm_chain,target_joint_pos);

    std::cout << "Forward Kinematic solution Matrix: " << std::endl;

    Eigen::MatrixXd Frame_Matrix(4, 4);
    Frame_Matrix << T(0, 0), T(0, 1), T(0, 2), T(0, 3),
                    T(1, 0), T(1, 1), T(1, 2), T(1, 3),
                    T(2, 0), T(2, 1), T(2, 2), T(2, 3),
                    T(3, 0), T(3, 1), T(3, 2), T(3, 3);

    std::cout << Frame_Matrix << std::endl;

    Eigen::Quaterniond forward_quaternion(Frame_Matrix.block<3, 3>(0, 0));
    std::cout << "Forward Quaternion" << std::endl;
    std::cout << "x: " << forward_quaternion.x() << " y: " << forward_quaternion.y() << " z: " << forward_quaternion.z() << " w: " << forward_quaternion.w() << std::endl;

    
    Eigen::MatrixXd error_matrix = Goal_Frame_Matrix - Frame_Matrix;

    std::cout << "error p1:" << error_matrix(0,3) << std::endl;
    std::cout << "error p2:" << error_matrix(1,3) << std::endl;
    std::cout << "error p3:" << error_matrix(2,3) << std::endl;

    }

    node.setParam("/mixamorig_RightArm_z", mixamorig_RightArm_z);
    node.setParam("/mixamorig_RightArm_x", mixamorig_RightArm_x);
    node.setParam("/mixamorig_RightArm_y", mixamorig_RightArm_y);
    node.setParam("/mixamorig_RightForeArm", mixamorig_RightForeArm);
    node.setParam("/mixamorig_RightHand_z", mixamorig_RightHand_z);
    node.setParam("/mixamorig_RightHand_x", mixamorig_RightHand_x);
    node.setParam("/mixamorig_RightHand_y", mixamorig_RightHand_y);

      

    if (!(token_name == "None")) {
        node.setParam("/finger_open", false);
        node.setParam("/grasp_hanoi_token_hanoi_tower_d300", token_name == "grasp_hanoi_token_hanoi_tower_d300" );
        node.setParam("/grasp_hanoi_token_hanoi_tower_d400", token_name == "grasp_hanoi_token_hanoi_tower_d400" );
        node.setParam("/grasp_hanoi_token_hanoi_tower_d500", token_name == "grasp_hanoi_token_hanoi_tower_d500" );
        node.setParam("/grasp_hanoi_token_hanoi_tower_d600", token_name == "grasp_hanoi_token_hanoi_tower_d600" );
        node.setParam("/grasp_hanoi_token_hanoi_tower_d700", token_name == "grasp_hanoi_token_hanoi_tower_d700" );

         bool trajectory_state_is_done = false ;
        while (!trajectory_state_is_done){
            node.getParam("/trajectory_state_is_done", trajectory_state_is_done);
            std::cout << "Waiting in while loop" << std::endl;


        }

        std::cout << "Finger opens" << std::endl;
        node.setParam("/finger_open", true);
    }

    else {

          bool trajectory_state_is_done = false ;
        while (!trajectory_state_is_done){
            node.getParam("/trajectory_state_is_done", trajectory_state_is_done);
            std::cout << "Waiting in while loop" << std::endl;


        }


    }

    
    node.setParam("/grasp_hanoi_token_hanoi_tower_d300", false );
    node.setParam("/grasp_hanoi_token_hanoi_tower_d400", false );
    node.setParam("/grasp_hanoi_token_hanoi_tower_d500", false );
    node.setParam("/grasp_hanoi_token_hanoi_tower_d600", false );
    node.setParam("/grasp_hanoi_token_hanoi_tower_d700", false );

}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "avatar_trajectory_follower");
    KDL::Tree my_tree;
    ros::NodeHandle node;
    string robot_desc_string;
    node.param("robot_description", robot_desc_string, string());
    if (!kdl_parser::treeFromString(robot_desc_string, my_tree))
    {
        ROS_ERROR("Failed to construct kdl tree");
        return false;
    }
    KDL::Chain kuka_arm_chain;
    my_tree.getChain("mixamorig_Hips", "mixamorig_RightHand", kuka_arm_chain);

    int number_of_joints = kuka_arm_chain.getNrOfJoints();
    ROS_WARN("Number of joints: %d\n", number_of_joints);


    KDL::Frame T_hips_to_world = Transformation_matrix_hips_to_world_frame();


    // This is global coordinate target frame , change this to give target pos
    // KDL::Vector pos_target = KDL::Vector(-0.15,-0.14,1.17);

    // std::vector<double> mixamorig_RightArm_z;
    // std::vector<double> mixamorig_RightArm_x;
    // std::vector<double> mixamorig_RightArm_y;
    // std::vector<double> mixamorig_RightForeArm;
    // std::vector<double> mixamorig_RightHand_z;
    // std::vector<double> mixamorig_RightHand_x;
    // std::vector<double> mixamorig_RightHand_y;
    
    std::cout << argc << std::endl;
    for (int i = 0; i < argc; ++i) {
        std::cout << argv[i] << std::endl;
    }

    // Create Trajectory
    std::vector<KDL::Vector> trajectory_points;
   
    KDL::Vector start_vec ;
    KDL::Vector end_vec ;
    std::string token_name;
    double y_coordinate_from = 0.0;
    double y_coordinate_to = 0.0;

     if (std::string(argv[2]) == "hanoi_board_middle_top_reference_frame"){
         y_coordinate_from = -0.180;
     }
     else if (std::string(argv[2]) == "hanoi_board_left_top_reference_frame"){
         y_coordinate_from = -0.300;
     }
     else if (std::string(argv[2]) == "hanoi_board_right_top_reference_frame"){
         y_coordinate_from = -0.060;
     }
     else {
         std::cout << "configuration is not found !!!" << std::endl;
     }

     if (std::string(argv[3]) == "hanoi_board_middle_top_reference_frame"){
         y_coordinate_to = -0.180;
     }
     else if (std::string(argv[3]) == "hanoi_board_left_top_reference_frame"){
         y_coordinate_to = -0.300;
     }
     else if (std::string(argv[3]) == "hanoi_board_right_top_reference_frame"){
         y_coordinate_to = -0.060;
     }
     else {
         std::cout << "configuration is not found !!!" << std::endl;
     }



    if (std::string(argv[1]) == "red"){
    
    trajectory_points.clear();
    start_vec = KDL::Vector(-0.75,y_coordinate_from, 0.5); 
    end_vec = KDL::Vector(-0.50,y_coordinate_from, 1.15); 
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.50,y_coordinate_from, 1.15); 
    end_vec = KDL::Vector(-0.20,y_coordinate_from, 1.25); 
    linspace(start_vec,end_vec,50,trajectory_points);


    start_vec = KDL::Vector(-0.20, y_coordinate_from, 1.25); 
    end_vec = KDL::Vector(-0.1171260324722303, y_coordinate_from, 1.16);
    linspace(start_vec,end_vec,50,trajectory_points);

    // start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from, 1.30); 
    // end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from, 1.16);
    // linspace(start_vec,end_vec,50,trajectory_points);

    pick_and_place_token(node,kuka_arm_chain,trajectory_points,"None");
    trajectory_points.clear();

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from, 1.16); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from, 1.20);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from, 1.20); 
    end_vec = KDL::Vector(-0.20,(y_coordinate_to+y_coordinate_from)/(double)2, 1.25);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.20,(y_coordinate_to+y_coordinate_from)/(double)2, 1.25); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to, 1.18);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to, 1.18); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to, 1.16);
    linspace(start_vec,end_vec,50,trajectory_points);

    token_name = "grasp_hanoi_token_hanoi_tower_d300";
    pick_and_place_token(node,kuka_arm_chain,trajectory_points,token_name);
    trajectory_points.clear();

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to, 1.16); 
    end_vec = KDL::Vector(-0.20,y_coordinate_to, 1.25);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.20,y_coordinate_to, 1.25); 
    end_vec = KDL::Vector(-0.50,-0.300, 1.15);
    linspace(start_vec,end_vec,100,trajectory_points);

    start_vec = KDL::Vector(-0.50,-0.300, 1.15); 
    end_vec = KDL::Vector(-0.75,-0.300, 0.5);
    linspace(start_vec,end_vec,50,trajectory_points);

    pick_and_place_token(node,kuka_arm_chain,trajectory_points,"None");

    }

    else if (std::string(argv[1]) == "yellow") {

    trajectory_points.clear();
    start_vec = KDL::Vector(-0.75,y_coordinate_from, 0.5); 
    end_vec = KDL::Vector(-0.5,y_coordinate_from, 1.15); 
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.5, y_coordinate_from, 1.15); 
    end_vec = KDL::Vector(-0.2, y_coordinate_from, 1.25);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.2,y_coordinate_from, 1.25); 
    end_vec = KDL::Vector(-0.11,y_coordinate_from, 1.14);
    linspace(start_vec,end_vec,50,trajectory_points);

    pick_and_place_token(node,kuka_arm_chain,trajectory_points,"None");
    trajectory_points.clear();

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from+0.01, 1.14); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from+0.01, 1.20);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from+0.01, 1.20); 
    end_vec = KDL::Vector(-0.2,(y_coordinate_to+y_coordinate_from)/(double)2, 1.25);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.2,(y_coordinate_to+y_coordinate_from)/(double)2, 1.25); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.01, 1.18);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.01, 1.18); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.01, 1.16);
    linspace(start_vec,end_vec,50,trajectory_points);

    token_name = "grasp_hanoi_token_hanoi_tower_d400";
    pick_and_place_token(node,kuka_arm_chain,trajectory_points,token_name);

    trajectory_points.clear();

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.01, 1.16); 
    end_vec = KDL::Vector(-0.2,y_coordinate_to + 0.01, 1.25);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.2,y_coordinate_to + 0.01, 1.25); 
    end_vec = KDL::Vector(-0.5,-0.3, 1.15);
    linspace(start_vec,end_vec,100,trajectory_points);

    start_vec = KDL::Vector(-0.5,-0.3, 1.15); 
    end_vec = KDL::Vector(-0.75,-0.3, 0.5);
    linspace(start_vec,end_vec,50,trajectory_points);

    pick_and_place_token(node,kuka_arm_chain,trajectory_points,"None");

    }

    else if (std::string(argv[1]) == "green"){

    trajectory_points.clear();
    start_vec = KDL::Vector(-0.75,y_coordinate_from, 0.5); 
    end_vec = KDL::Vector(-0.5,y_coordinate_from, 1.15); 
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.5, y_coordinate_from, 1.15); 
    end_vec = KDL::Vector(-0.2, y_coordinate_from, 1.25);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.2,y_coordinate_from, 1.25); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from, 1.12);
    linspace(start_vec,end_vec,50,trajectory_points);

    pick_and_place_token(node,kuka_arm_chain,trajectory_points,"None");
    trajectory_points.clear();

     start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from + 0.02, 1.12); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from + 0.02, 1.20);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from + 0.02, 1.20); 
    end_vec = KDL::Vector(-0.2,(y_coordinate_to+y_coordinate_from)/(double)2, 1.25);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.2,(y_coordinate_to+y_coordinate_from)/(double)2, 1.25); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.02, 1.18);
    linspace(start_vec,end_vec,50,trajectory_points);


    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.02, 1.18); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.02, 1.16);
    linspace(start_vec,end_vec,50,trajectory_points);

    token_name = "grasp_hanoi_token_hanoi_tower_d500";
    pick_and_place_token(node,kuka_arm_chain,trajectory_points,token_name);
    trajectory_points.clear();

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.02, 1.16); 
    end_vec = KDL::Vector(-0.2,y_coordinate_to + 0.02, 1.25);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.2,y_coordinate_to + 0.02, 1.25); 
    end_vec = KDL::Vector(-0.5,-0.3, 1.15);
    linspace(start_vec,end_vec,100,trajectory_points);

    start_vec = KDL::Vector(-0.5,-0.3, 1.15); 
    end_vec = KDL::Vector(-0.75,-0.3, 0.5);
    linspace(start_vec,end_vec,50,trajectory_points);

    pick_and_place_token(node,kuka_arm_chain,trajectory_points,"None");
    }

    else if (std::string(argv[1]) == "blue"){

        trajectory_points.clear();

        start_vec = KDL::Vector(-0.75,y_coordinate_from, 0.5); 
        end_vec = KDL::Vector(-0.5,y_coordinate_from, 1.15); 
        linspace(start_vec,end_vec,50,trajectory_points);

        start_vec = KDL::Vector(-0.5, y_coordinate_from, 1.15); 
        end_vec = KDL::Vector(-0.2, y_coordinate_from, 1.25);
        linspace(start_vec,end_vec,50,trajectory_points);

        start_vec = KDL::Vector(-0.2, y_coordinate_from, 1.25); 
        end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from, 1.16);
        linspace(start_vec,end_vec,50,trajectory_points);

        pick_and_place_token(node,kuka_arm_chain,trajectory_points,"None");
        trajectory_points.clear();

        start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from + 0.03, 1.10); 
        end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from + 0.03, 1.20);
        linspace(start_vec,end_vec,50,trajectory_points);

        start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from + 0.03, 1.20); 
        end_vec = KDL::Vector(-0.2,(y_coordinate_to+y_coordinate_from)/(double)2, 1.25);
        linspace(start_vec,end_vec,50,trajectory_points);

        start_vec = KDL::Vector(-0.2,(y_coordinate_to+y_coordinate_from)/(double)2, 1.25); 
        end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.03, 1.18);
        linspace(start_vec,end_vec,50,trajectory_points);

        start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.03, 1.18); 
        end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.03, 1.16);
        linspace(start_vec,end_vec,50,trajectory_points);

        token_name = "grasp_hanoi_token_hanoi_tower_d600";
        pick_and_place_token(node,kuka_arm_chain,trajectory_points,token_name);
        trajectory_points.clear();

        start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to + 0.03, 1.16); 
        end_vec = KDL::Vector(-0.2,y_coordinate_to + 0.03, 1.25);
        linspace(start_vec,end_vec,50,trajectory_points);

        start_vec = KDL::Vector(-0.2,y_coordinate_to + 0.03, 1.25); 
        end_vec = KDL::Vector(-0.5,-0.3, 1.15);
        linspace(start_vec,end_vec,100,trajectory_points);

        start_vec = KDL::Vector(-0.5,-0.300, 1.15); 
        end_vec = KDL::Vector(-0.75,-0.300, 0.5);
        linspace(start_vec,end_vec,50,trajectory_points);

        pick_and_place_token(node,kuka_arm_chain,trajectory_points,"None");

    }

    else if (std::string(argv[1]) == "pink") {
         trajectory_points.clear();

     start_vec = KDL::Vector(-0.75,y_coordinate_from, 0.5); 
        end_vec = KDL::Vector(-0.5,y_coordinate_from, 1.15); 
        linspace(start_vec,end_vec,50,trajectory_points);

        start_vec = KDL::Vector(-0.5, y_coordinate_from, 1.15); 
        end_vec = KDL::Vector(-0.2, y_coordinate_from, 1.25);
        linspace(start_vec,end_vec,50,trajectory_points);

        start_vec = KDL::Vector(-0.2,y_coordinate_from, 1.25); 
        end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from, 1.16);
        linspace(start_vec,end_vec,50,trajectory_points);

        pick_and_place_token(node,kuka_arm_chain,trajectory_points,"None");
        trajectory_points.clear();
             
     start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from + 0.04, 1.08); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from + 0.04, 1.20);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_from + 0.04, 1.20); 
    end_vec = KDL::Vector(-0.2,(y_coordinate_to+y_coordinate_from)/(double)2, 1.30);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.2,(y_coordinate_to+y_coordinate_from)/(double)2, 1.20); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to+0.04, 1.18);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to+0.04, 1.18); 
    end_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to+0.04, 1.16);
    linspace(start_vec,end_vec,50,trajectory_points);

    token_name = "grasp_hanoi_token_hanoi_tower_d700";
    pick_and_place_token(node,kuka_arm_chain,trajectory_points,token_name);


    trajectory_points.clear();

    start_vec = KDL::Vector(-0.1171260324722303,y_coordinate_to+0.04, 1.16); 
    end_vec = KDL::Vector(-0.2,y_coordinate_to+0.04, 1.25);
    linspace(start_vec,end_vec,50,trajectory_points);

    start_vec = KDL::Vector(-0.2,y_coordinate_to+0.04, 1.25); 
    end_vec = KDL::Vector(-0.5,-0.3, 1.15);
    linspace(start_vec,end_vec,100,trajectory_points);

    start_vec = KDL::Vector(-0.5,-0.300, 1.15); 
    end_vec = KDL::Vector(-0.75,-0.300, 0.5);
    linspace(start_vec,end_vec,50,trajectory_points);

    pick_and_place_token(node,kuka_arm_chain,trajectory_points,"None");

    }

    else {
        std::cout << "Argument is not found!!!" << std::endl;
        std::cout << "Have " << argc << " arguments:" << std::endl;
        for (int i = 0; i < argc; ++i) {
            std::cout << argv[i] << std::endl;
        }
    }


    // for (int i = 0; i<trajectory_points.size() ; i++) {
    // // KDL::Vector pos_target = KDL::Vector(0.1,-0.24-0.005*i,1.17);
    // KDL::Rotation rot_target;
    // rot_target = rot_target.Quaternion(0,0,1,0);
    // KDL::Frame T_goal = go_target_position(trajectory_points[i],rot_target);

    // Eigen::MatrixXd Goal_Frame_Matrix(4, 4);
    // Goal_Frame_Matrix << T_goal(0, 0), T_goal(0, 1), T_goal(0, 2), T_goal(0, 3),
    //                      T_goal(1, 0), T_goal(1, 1), T_goal(1, 2), T_goal(1, 3),
    //                      T_goal(2, 0), T_goal(2, 1), T_goal(2, 2), T_goal(2, 3),
    //                      T_goal(3, 0), T_goal(3, 1), T_goal(3, 2), T_goal(3, 3);

    // std::cout << "Goal Transformation Matrix: " << std::endl;
    // std::cout << Goal_Frame_Matrix << std::endl;


    // KDL::JntArray target_joint_pos  =  inverse_kinematic_solver(kuka_arm_chain,number_of_joints,T_goal);


    // std::cout << target_joint_pos.rows() << std::endl;


    // mixamorig_RightArm_z.push_back( target_joint_pos(0));
    // mixamorig_RightArm_x.push_back( target_joint_pos(1));
    // mixamorig_RightArm_y.push_back( target_joint_pos(2));
    // mixamorig_RightForeArm.push_back( target_joint_pos(3));
    // mixamorig_RightHand_z.push_back( target_joint_pos(4));
    // mixamorig_RightHand_x.push_back( target_joint_pos(5));
    // mixamorig_RightHand_y.push_back( target_joint_pos(6));

    // node.setParam("/grasp_hanoi_token_hanoi_tower_d300", true);
    // node.setParam("/grasp_hanoi_token_hanoi_tower_d400", false);
    // node.setParam("/grasp_hanoi_token_hanoi_tower_d500", false);
    // node.setParam("/grasp_hanoi_token_hanoi_tower_d600", false);
    // node.setParam("/grasp_hanoi_token_hanoi_tower_d700", false);


    // node.setParam("/hand_close_avatar", true);


    // std::cout << "Verify the solution with Forward Kinematics" << std::endl;

    // KDL::Frame T = forward_kinematic_solver(kuka_arm_chain,target_joint_pos);

    // std::cout << "Forward Kinematic solution Matrix: " << std::endl;

    // Eigen::MatrixXd Frame_Matrix(4, 4);
    // Frame_Matrix << T(0, 0), T(0, 1), T(0, 2), T(0, 3),
    //                 T(1, 0), T(1, 1), T(1, 2), T(1, 3),
    //                 T(2, 0), T(2, 1), T(2, 2), T(2, 3),
    //                 T(3, 0), T(3, 1), T(3, 2), T(3, 3);

    // std::cout << Frame_Matrix << std::endl;

    // Eigen::Quaterniond forward_quaternion(Frame_Matrix.block<3, 3>(0, 0));
    // std::cout << "Forward Quaternion" << std::endl;
    // std::cout << "x: " << forward_quaternion.x() << " y: " << forward_quaternion.y() << " z: " << forward_quaternion.z() << " w: " << forward_quaternion.w() << std::endl;

    
    // Eigen::MatrixXd error_matrix = Goal_Frame_Matrix - Frame_Matrix;

    // std::cout << "error p1:" << error_matrix(0,3) << std::endl;
    // std::cout << "error p2:" << error_matrix(1,3) << std::endl;
    // std::cout << "error p3:" << error_matrix(2,3) << std::endl;

    // }

    // node.setParam("/mixamorig_RightArm_z", mixamorig_RightArm_z);
    // node.setParam("/mixamorig_RightArm_x", mixamorig_RightArm_x);
    // node.setParam("/mixamorig_RightArm_y", mixamorig_RightArm_y);
    // node.setParam("/mixamorig_RightForeArm", mixamorig_RightForeArm);
    // node.setParam("/mixamorig_RightHand_z", mixamorig_RightHand_z);
    // node.setParam("/mixamorig_RightHand_x", mixamorig_RightHand_x);
    // node.setParam("/mixamorig_RightHand_y", mixamorig_RightHand_y);
    // node.setParam("/counter_reset", true);

    // node.setParam("/finger_open", false);

    // SimulationDelay(23);
    // std::cout << "Finger opens" << std::endl;
    // node.setParam("/finger_open", true);
    // node.setParam("/grasp_hanoi_token", false);






}
