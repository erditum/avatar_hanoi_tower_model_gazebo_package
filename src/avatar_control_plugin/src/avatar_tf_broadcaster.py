#!/usr/bin/python
import rospy
from std_msgs.msg import String
from gazebo_msgs.msg import LinkStates
import tf
import numpy as np
from math import pi
#  This matchers is used to find how many ea_robots there are in the Gazebo
obj_starts_with = "avatar_ybot"

def avatar_link_tf_publisher(gazebo_data):
    object_list_in_gazebo = gazebo_data.name
    all_objects_startswith = [i for i in gazebo_data.name if i.startswith(obj_starts_with)] # getting all the links related to the avatar ybot
    for avatar_link in all_objects_startswith:
        index_number = object_list_in_gazebo.index(avatar_link)
        avatar_link_pos = gazebo_data.pose[index_number].position
        avatar_link_ori = gazebo_data.pose[index_number].orientation
        br = tf.TransformBroadcaster()
        br.sendTransform((avatar_link_pos.x,avatar_link_pos.y,avatar_link_pos.z),
                            (avatar_link_ori.x, avatar_link_ori.y, avatar_link_ori.z, avatar_link_ori.w),
                            rospy.Time.now(),
                            avatar_link[len(obj_starts_with) + 2 :],
                            "world")

def shutdownhook():
    print('System is being closed')

def callback(data):
    avatar_link_tf_publisher(gazebo_data = data)
    # Create gazebo referece frame from the model or hips frame because it is 0 position for the model but not for the gazebo
    # k_ind = data.name.index("avatar_ybot::mixamorig_Hips")

    # br1 = tf.TransformBroadcaster()
    # br1.sendTransform((-data.pose[k_ind].position.x,-data.pose[k_ind].position.y,-data.pose[k_ind].position.z),
    #                      (0, 0, 0, 1),
    #                      rospy.Time.now(),
    #                      'gazebo_reference',
    #                      "world")


if __name__ == "__main__":
    rospy.init_node('hanoi_tower_reference_frame_broadcaster')
    rospy.logwarn("ROS TF Broadcaster is initialized...")
    # rospy.sleep(4)
    rospy.Subscriber("/gazebo/link_states", LinkStates, callback)
    rospy.Rate(10)
    rospy.on_shutdown(shutdownhook)
    rospy.spin()
