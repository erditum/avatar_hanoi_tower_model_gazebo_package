#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>
#include <ros/ros.h>
#include <unistd.h>
#include <ctime>
#include <chrono>
#include <map>
#include <cmath> // std::abs
#include <string>

namespace gazebo
{
  class ModelPush : public ModelPlugin
  {
  public:
    void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
      // Store the pointer to the model
      this->model = _parent;

      gazebo::physics::Joint_V joints = this->model->GetJoints();
      std::cout << "Number of joints: " << std::endl;
      std::cout << joints.size() << std::endl; // size of joints vector should be number of joints.
      for (int i = 0; i < joints.size(); i++)
      {
        // store all the joints name  and joint pointer into the map
        joint_map[joints[i]->GetName()] = joints[i];
      }

      

      // Listen to the update event. This event is broadcast every
      // simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&ModelPush::OnUpdate, this));

      ROS_WARN("Loaded ModelPush Plugin with parent...%s", this->model->GetName().c_str());
    }

    // Called by the world update start event
  public:
    void OnUpdate()
    {
      // Apply a small linear velocity to the model.
      // this->model->SetLinearVel(ignition::math::Vector3d(.3, 0, 0));

      std::cout << "Before Sleep" << std::endl;
      // Get current time
      auto end = std::chrono::system_clock::now();
      std::time_t end_time = std::chrono::system_clock::to_time_t(end);
      std::cout << "finished computation at " << std::ctime(&end_time);
      usleep(1000000 / 10); // 1 second = 1e6 micro seconds
      std::cout << "After Sleep" << std::endl;

      // counter = counter + 1 ;
      // int steps = 10;

      // for (const std::string j : animate_joints){
      //   this->animate(joint_map[j],steps,counter);
      // }

      // double mixamorig_RightArm_x;
      // node.getParam("/mixamorig_RightArm_x",mixamorig_RightArm_x);
      // joint_map["mixamorig_RightArm_x"]->SetPosition(0,mixamorig_RightArm_x);

      // double mixamorig_RightArm_y;
      // node.getParam("/mixamorig_RightArm_y",mixamorig_RightArm_y);
      // joint_map["mixamorig_RightArm_y"]->SetPosition(0,mixamorig_RightArm_y);

      // double mixamorig_RightArm_z;
      // node.getParam("/mixamorig_RightArm_z",mixamorig_RightArm_z);
      // joint_map["mixamorig_RightArm_z"]->SetPosition(0,mixamorig_RightArm_z);

      // double mixamorig_RightForeArm;
      // node.getParam("/mixamorig_RightForeArm",mixamorig_RightForeArm);
      // joint_map["mixamorig_RightForeArm"]->SetPosition(0,mixamorig_RightForeArm);

      // double mixamorig_RightHand_z;
      // node.getParam("/mixamorig_RightHand_z",mixamorig_RightHand_z);
      // joint_map["mixamorig_RightHand_z"]->SetPosition(0,mixamorig_RightHand_z);

      // double mixamorig_RightHand_x;
      // node.getParam("/mixamorig_RightHand_x",mixamorig_RightHand_x);
      // joint_map["mixamorig_RightHand_x"]->SetPosition(0,mixamorig_RightHand_x);

      // double mixamorig_RightHand_y;
      // node.getParam("/mixamorig_RightHand_y",mixamorig_RightHand_y);
      // joint_map["mixamorig_RightHand_y"]->SetPosition(0,mixamorig_RightHand_y);


      this->jointController.reset(new physics::JointController(
                this->model));
          // this->jointController->AddJoint(model->GetJoint("purple_joint"));
          std::string name = model->GetJoint("mixamorig_RightArm_z")->GetScopedName();
          this->jointController->SetPositionPID(name, common::PID(100, 0, 0));
          // std::cout << name << std::endl;
          this->jointController->SetPositionTarget(name, 0.581514);

      // double mixamorig_RightHandMiddle1;
      // node.getParam("/mixamorig_RightHandMiddle1",mixamorig_RightHandMiddle1);
      // joint_map["mixamorig_RightHandMiddle1"]->SetPosition(0,mixamorig_RightHandMiddle1);

      // double mixamorig_RightHandMiddle2;
      // node.getParam("/mixamorig_RightHandMiddle2",mixamorig_RightHandMiddle2);
      // joint_map["mixamorig_RightHandMiddle2"]->SetPosition(0,mixamorig_RightHandMiddle2);

      // double mixamorig_RightHandMiddle3;
      // node.getParam("/mixamorig_RightHandMiddle3",mixamorig_RightHandMiddle3);
      // joint_map["mixamorig_RightHandMiddle3"]->SetPosition(0,mixamorig_RightHandMiddle3);

      // joint_map["mixamorig_RightArm_z"]->SetPosition(0,1.48975);
      // joint_map["mixamorig_RightArm_x"]->SetPosition(0,-0.049361);
      // joint_map["mixamorig_RightArm_y"]->SetPosition(0,1.23661);
      // joint_map["mixamorig_RightForeArm"]->SetPosition(0,0.745961);
      // joint_map["mixamorig_RightHand_z"]->SetPosition(0,0.876941);
      // joint_map["mixamorig_RightHand_x"]->SetPosition(0,-2.80746);
      // joint_map["mixamorig_RightHand_y"]->SetPosition(0,0.910221);
      // joint_map["mixamorig_RightHandMiddle1"]->SetPosition(0,0.702452);
      // joint_map["mixamorig_RightHandMiddle2"]->SetPosition(0,0.715103);
      // joint_map["mixamorig_RightHandMiddle3"]->SetPosition(0,0.749807);

      joint_map["mixamorig_Spine"]->SetPosition(0, 0);
      joint_map["mixamorig_Spine1"]->SetPosition(0, 0);
      joint_map["mixamorig_Spine2"]->SetPosition(0, 0);
      joint_map["mixamorig_Neck"]->SetPosition(0, 0);
      joint_map["mixamorig_Head"]->SetPosition(0, 0);
      joint_map["mixamorig_HeadTop_End"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftEye"]->SetPosition(0, 0);
      joint_map["mixamorig_RightEye"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftShoulder"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftArm_z"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftArm_x"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftArm_y"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftForeArm"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHand_z"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHand_x"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHand_y"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandMiddle1"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandMiddle2"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandMiddle3"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandMiddle4"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandThumb1_z"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandThumb1_x"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandThumb1_y"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandThumb2"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandThumb3"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandThumb4"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandIndex1"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandIndex2"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandIndex3"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandIndex4"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandRing1"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandRing2"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandRing3"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandRing4"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandPinky1"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandPinky2"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandPinky3"]->SetPosition(0, 0);
      joint_map["mixamorig_LeftHandPinky4"]->SetPosition(0, 0);
      joint_map["mixamorig_RightHandThumb1_y"]->SetPosition(0, +1.57);



      bool hand_close_avatar;
      node.getParam("/hand_close_avatar",hand_close_avatar);
      if (hand_close_avatar){
      this->rightHand_close();

      }
    }

  public:
    void animate(gazebo::physics::JointPtr &joint, int steps, int counter)
    {
      double pos_val; // position value from the ros param server
      node.getParam("/" + joint->GetName(), pos_val);
      if (std::abs(pos_val) > std::abs(pos_val / double(steps) * counter))
      {

        joint->SetPosition(0, pos_val / double(steps) * counter);
      }
    }

    void rightHand_close()
    {
      std::cout << "Right Hand close" << std::endl;
      joint_map["mixamorig_RightHandIndex1_z"]->SetPosition(0, 1.24);
      joint_map["mixamorig_RightHandIndex2_z"]->SetPosition(0, 1.57);
      joint_map["mixamorig_RightHandIndex3_z"]->SetPosition(0, 0.90);

      joint_map["mixamorig_RightHandMiddle1_z"]->SetPosition(0, 1.24);
      joint_map["mixamorig_RightHandMiddle2_z"]->SetPosition(0, 1.57);
      joint_map["mixamorig_RightHandMiddle3_z"]->SetPosition(0, 0.90);

      joint_map["mixamorig_RightHandRing1_z"]->SetPosition(0, 1.24);
      joint_map["mixamorig_RightHandRing2_z"]->SetPosition(0, 1.57);
      joint_map["mixamorig_RightHandRing3_z"]->SetPosition(0, 0.90);

      joint_map["mixamorig_RightHandPinky1_z"]->SetPosition(0, 1.24);
      joint_map["mixamorig_RightHandPinky2_z"]->SetPosition(0, 1.57);
      joint_map["mixamorig_RightHandPinky3_z"]->SetPosition(0, 0.90);

      // joint_map["mixamorig_RightHandThumb2"]->SetPosition(0, -0.65);
      // joint_map["mixamorig_RightHandThumb3"]->SetPosition(0, -1.57);
    }

  private:
    std::vector<std::string> animate_joints = {
        "mixamorig_RightArm_z", "mixamorig_RightArm_y", "mixamorig_RightArm_x",
        "mixamorig_RightForeArm", "mixamorig_RightHand_z", "mixamorig_RightHand_x",
        "mixamorig_RightHand_y"};

  private:
    int counter = 0;
    // Pointer to the model
  private:
    physics::ModelPtr model;

  private:
    ros::NodeHandle node;

  private:
    std::map<std::string, gazebo::physics::JointPtr> joint_map;

    // Pointer to the update event connection
  private:
    event::ConnectionPtr updateConnection;

  public: physics::JointControllerPtr jointController;

  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ModelPush)
} // namespace gazebo