#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>
#include <gazebo_msgs/LinkStates.h>

#include <ros/ros.h>
#include "std_msgs/String.h"
// void chatterCallback(const gazebo_msgs::LinkStates link_states)
// {
//   std::cout << link_states.name[45] << std::endl;
// }
namespace gazebo
{
  class ModelPush : public ModelPlugin
  {
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
      // Store the pointer to the model
      this->model = _parent;
      ROS_WARN("Hanoi model plugin is loaded for %s", this->model->GetName().c_str());
      mixamorig_RightHandIndex1_z =  _sdf->GetElement("mixamorig_RightHandIndex1_z");
      mixamorig_RightHandIndex2_z =  _sdf->GetElement("mixamorig_RightHandIndex2_z");
      mixamorig_RightHandIndex3_z =  _sdf->GetElement("mixamorig_RightHandIndex3_z");
      token_radius_tag =  _sdf->GetElement("token_radius");
      token_offset = token_radius_tag->Get<float>();


      if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = nullptr;
        ros::init(argc, argv, "JSP", ros::init_options::NoSigintHandler);
        // DebugMessage("ROS Node is initialized manually.", WHITE_TXT);
    }
    this->rosNode_unique_ptr = std::make_unique<ros::NodeHandle>(this->model->GetName());
    // this->rosPubJointStates = this->rosNode_unique_ptr->advertise<std_msgs::String>("chatter", 1000);
    this->sub = this->rosNode_unique_ptr->subscribe("/gazebo/link_states", 1000, &gazebo::ModelPush::chatterCallback,this);
    // this->timer = this->rosNode_unique_ptr->createTimer(ros::Duration(1.0 / 50.), &gazebo::ModelPush::test_fun,this); 
      // Listen to the update event. This event is broadcast every
      // simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&ModelPush::OnUpdate, this));
    }


    public: void chatterCallback(const gazebo_msgs::LinkStates link_states)
    {
      std::string based_link_name = "avatar_ybot::mixamorig_RightHandThumb4";
      if (link_index_base == -1){

        for (int i = 0 ; i < link_states.name.size(); i++){

            if (link_states.name[i] == based_link_name ){
              link_index_base = i;
            }
            else{
              std::cout << "Based link is not found " << std::endl;
            }

        }

      }
      else {
            // std::cout << link_states.name[link_index_base] << std::endl;
            this->position = ignition::math::Vector3d(link_states.pose[link_index_base].position.x,link_states.pose[link_index_base].position.y, link_states.pose[link_index_base].position.z);
            this->quat = ignition::math::Quaterniond(1,0,0,0);
      }

    }

    public: void test_fun(const ros::TimerEvent &event) {
      std_msgs::String msg;
      msg.data = "Hello";

      this->rosPubJointStates.publish(msg);

    }

    // Called by the world update start event
    public: void OnUpdate()
    {
      // Apply a small linear velocity to the model.
      // this->model->SetLinearVel(ignition::math::Vector3d(.3, 0, 0));
      // ignition::math::Pose3d mypose = ignition::math::Pose3d(.3, 0, 0.5);
      // ignition::math::Vector3d position = ignition::math::Vector3d(0, 1, 1);

      ignition::math::Pose3d target_loc = ignition::math::Pose3d(position,quat)
      *ignition::math::Pose3d(ignition::math::Vector3d(0,-token_offset,0),ignition::math::Quaterniond(1,0,0,0)); //shift radius of token in negative y direction

      // Should be only 1 link in tokens
       std::vector<physics::LinkPtr> token_all_links = this->model->GetLinks();
      //  std::cout << token_all_links[0]->GetName() << std::endl;


       node.getParam("/grasp_hanoi_token_"+this->model->GetName(), grasp_hanoi_token);
      if (grasp_hanoi_token){

          // Load required finger angles for token into the rosparameter server 
          node.setParam("/mixamorig_RightHandIndex1_z",mixamorig_RightHandIndex1_z->Get<float>());
          node.setParam("/mixamorig_RightHandIndex2_z", mixamorig_RightHandIndex2_z->Get<float>());
          node.setParam("/mixamorig_RightHandIndex3_z", mixamorig_RightHandIndex3_z->Get<float>());

        this->model->SetLinkWorldPose(target_loc,token_all_links[0]->GetName());
        std::cout << target_loc.Pos() << std::endl;
        this->model->SetGravityMode(false);
        this->model->SetStatic(true);
      }
      else {
        this->model->SetGravityMode(true);
        this->model->SetStatic(false);

      }

    }

    private:
    // Pointer to the model
     physics::ModelPtr model;
    // Pointer to the update event connection
    event::ConnectionPtr updateConnection;
    std::unique_ptr<ros::NodeHandle> rosNode_unique_ptr;
    ros::Publisher rosPubJointStates, rosPubJointStates2;
    ros::Subscriber sub;
    ros::Timer timer;
    ignition::math::Vector3d position;
    ignition::math::Quaterniond quat;

    int link_index_base = -1;

    //Ros handle
    ros::NodeHandle node;

    bool grasp_hanoi_token;
    
    sdf::ElementPtr token_radius_tag;
    sdf::ElementPtr mixamorig_RightHandIndex1_z;
    sdf::ElementPtr mixamorig_RightHandIndex2_z;
    sdf::ElementPtr mixamorig_RightHandIndex3_z;

    double token_offset;
    

  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ModelPush)
}