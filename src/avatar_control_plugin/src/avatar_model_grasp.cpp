#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>
#include <avatar_grasp_plugin.h>

// #include <ros/ros.h> //For ROS_WARN
#include <unistd.h>
#include <ctime>
#include <chrono>
#include <cmath> // std::abs

using namespace gazebo;

AvatarModelGrasp::AvatarModelGrasp(){
    std::cout << "Constructor is called" << std::endl;
}
void AvatarModelGrasp::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
      // Store the pointer to the model
      this->model = _parent;

      // Listen to the update event. This event is broadcast every
      // simulation iteration.


      this->model->SetGravityMode(false);
      this->GetAllJoints();

      // Setting for left arm and hand
      m_joints["avatar_ybot::mixamorig_LeftArm_z"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftArm_x"]->SetPosition(0, 1.2);
      m_joints["avatar_ybot::mixamorig_LeftArm_y"]->SetPosition(0, -1.57);
      m_joints["avatar_ybot::mixamorig_LeftForeArm"]->SetPosition(0, 0.8);
      m_joints["avatar_ybot::mixamorig_LeftHand_z"]->SetPosition(0, 0.3);
      m_joints["avatar_ybot::mixamorig_LeftHand_x"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHand_y"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandThumb1_z"]->SetPosition(0, 0.7);




      // right hand position on the table
      // m_joints["avatar_ybot::mixamorig_RightArm_x"]->SetPosition(0, 1.92);
      // m_joints["avatar_ybot::mixamorig_RightArm_y"]->SetPosition(0, 1.3358);
      // m_joints["avatar_ybot::mixamorig_RightArm_z"]->SetPosition(0, 0);
      // m_joints["avatar_ybot::mixamorig_RightForeArm"]->SetPosition(0, -1.7);
      // m_joints["avatar_ybot::mixamorig_RightHand_z"]->SetPosition(0, -0.2);
      // m_joints["avatar_ybot::mixamorig_RightHand_x"]->SetPosition(0, -0.4);
      // m_joints["avatar_ybot::mixamorig_RightHand_y"]->SetPosition(0, 0);
      // m_joints["avatar_ybot::mixamorig_RightHandThumb1_z"]->SetPosition(0, -0.55);


      
      m_joints["avatar_ybot::mixamorig_RightArm_z"]->SetPosition(0, 1.43);
      m_joints["avatar_ybot::mixamorig_RightArm_x"]->SetPosition(0, -0.167281);
      m_joints["avatar_ybot::mixamorig_RightArm_y"]->SetPosition(0, -0.246179);
      m_joints["avatar_ybot::mixamorig_RightForeArm"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_RightHand_z"]->SetPosition(0, 0.102832);
      m_joints["avatar_ybot::mixamorig_RightHand_x"]->SetPosition(0, 0.194629);
      m_joints["avatar_ybot::mixamorig_RightHand_y"]->SetPosition(0, 1.78858);

      
      // node.setParam("/mixamorig_RightHandIndex1_z", 0.7);
      // node.setParam("/mixamorig_RightHandIndex2_z", 0.8);
      // node.setParam("/mixamorig_RightHandIndex3_z", 0.6);
      std::vector<double> empty_vector;
      node.setParam("/mixamorig_RightArm_z", empty_vector);
      node.setParam("/mixamorig_RightArm_x", empty_vector);
      node.setParam("/mixamorig_RightArm_y", empty_vector);
      node.setParam("/mixamorig_RightForeArm", empty_vector);
      node.setParam("/mixamorig_RightHand_z", empty_vector);
      node.setParam("/mixamorig_RightHand_x", empty_vector);
      node.setParam("/mixamorig_RightHand_y", empty_vector);

      // node.setParam("/mixamorig_LeftArm_z", 0);
      // node.setParam("/mixamorig_LeftArm_x", 1.2);
      // node.setParam("/mixamorig_LeftArm_y", -1.57);
      // node.setParam("/mixamorig_LeftForeArm", 0.8);
      // node.setParam("/mixamorig_LeftHand_z", 0.3);
      // node.setParam("/mixamorig_LeftHand_x", 0);
      // node.setParam("/mixamorig_LeftHand_y", 0);
      // node.setParam("/mixamorig_LeftHandThumb1_z", 0.7);
      node.deleteParam("/finger_open");
      node.deleteParam("/grasp_hanoi_token_hanoi_tower_d300");
      node.deleteParam("/grasp_hanoi_token_hanoi_tower_d400");
      node.deleteParam("/grasp_hanoi_token_hanoi_tower_d500");
      node.deleteParam("/grasp_hanoi_token_hanoi_tower_d600");
      node.deleteParam("/grasp_hanoi_token_hanoi_tower_d700");
      
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&AvatarModelGrasp::OnUpdate, this));
    }

  void AvatarModelGrasp::OnUpdate()
    {
      // this->SimulationDelay(0.1);
      counter++;


  

    //   this->ControllerSpecificJoint("avatar_ybot::mixamorig_RightArm_z",1.68541,common::PID(100, 0, 0));
    //   this->ControllerSpecificJoint("avatar_ybot::mixamorig_RightArm_x",0.325252,common::PID(100, 0, 0));
    //   this->ControllerSpecificJoint("avatar_ybot::mixamorig_RightArm_y",0.91899,common::PID(100, 0, 0));
    //   this->ControllerSpecificJoint("avatar_ybot::mixamorig_RightForeArm",0,common::PID(100, 0, 0));
    //   this->ControllerSpecificJoint("avatar_ybot::mixamorig_RightHand_z",-1.97198,common::PID(100, 0, 0));
    //   this->ControllerSpecificJoint("avatar_ybot::mixamorig_RightHand_x",-0.612375,common::PID(100, 0, 0));
    //   this->ControllerSpecificJoint("avatar_ybot::mixamorig_RightHand_y",-0.124551,common::PID(100, 0, 0));
      

      // double f_pos;
      // node.getParam("/mixamorig_RightHandThumb1_z", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb1_z"]->SetPosition(0, f_pos);
      // node.getParam("/mixamorig_RightHandThumb1_x", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb1_x"]->SetPosition(0, f_pos);
      // node.getParam("/mixamorig_RightHandThumb1_y", f_pos); 
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb1_y"]->SetPosition(0, f_pos);
      //   node.getParam("/mixamorig_RightHandThumb2", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb2"]->SetPosition(0, f_pos);

      // node.getParam("/mixamorig_RightHandIndex1_z", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandIndex1_z"]->SetPosition(0, f_pos);
      //  node.getParam("/mixamorig_RightHandIndex2_z", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandIndex2_z"]->SetPosition(0, f_pos);
      //  node.getParam("/mixamorig_RightHandIndex3_z", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandIndex3_z"]->SetPosition(0, f_pos);
    
      // m_joints["avatar_ybot::mixamorig_RightArm_x"]->SetPosition(0, 0.325252);
      // m_joints["avatar_ybot::mixamorig_RightArm_y"]->SetPosition(0, 0.91899);
      // m_joints["avatar_ybot::mixamorig_RightForeArm"]->SetPosition(0, 0);
      // m_joints["avatar_ybot::mixamorig_RightHand_z"]->SetPosition(0, -1.97198);
      // m_joints["avatar_ybot::mixamorig_RightHand_x"]->SetPosition(0, -0.612375);
      // m_joints["avatar_ybot::mixamorig_RightHand_y"]->SetPosition(0, -0.124551);
      
      

      if (node.hasParam("/finger_open")){
          std::cout << "Rosparam settings no param" << std::endl;
          // can be deleted from here to 
           std::cout << "-------------------------" << std::endl;
           std::cout << m_joints["avatar_ybot::mixamorig_RightArm_z"]->Position(0) << std::endl;
           std::cout << m_joints["avatar_ybot::mixamorig_RightArm_x"]->Position(0) << std::endl;
           std::cout << m_joints["avatar_ybot::mixamorig_RightArm_y"]->Position(0) << std::endl;
           std::cout << m_joints["avatar_ybot::mixamorig_RightForeArm"]->Position(0) << std::endl;
           std::cout << m_joints["avatar_ybot::mixamorig_RightHand_z"]->Position(0) << std::endl;
           std::cout << m_joints["avatar_ybot::mixamorig_RightHand_x"]->Position(0) << std::endl;
           std::cout << m_joints["avatar_ybot::mixamorig_RightHand_y"]->Position(0) << std::endl;
           std::cout << "-------------------------" << std::endl;

          // here



          bool finger_pos;
          node.getParam("/finger_open", finger_pos);
          if (finger_pos){
            finger_open();
          }
          else { 
            finger_close();
          }
      }
      else {
            std::cout << "Rosparam settings for joints" << std::endl;
            
        //     double mixamorig_RightArm_z,mixamorig_RightArm_x,mixamorig_RightArm_y,mixamorig_RightForeArm,mixamorig_RightHand_z,mixamorig_RightHand_x,mixamorig_RightHand_y,mixamorig_RightHandThumb1_z;
        //   node.getParam("/RightArm_z", mixamorig_RightArm_z);
        //   m_joints["avatar_ybot::mixamorig_RightArm_z"]->SetPosition(0, mixamorig_RightArm_z);
        //  node.getParam("/RightArm_x", mixamorig_RightArm_x); 
        //   m_joints["avatar_ybot::mixamorig_RightArm_x"]->SetPosition(0, mixamorig_RightArm_x);
        //   node.getParam("/RightArm_y", mixamorig_RightArm_y);
        //   m_joints["avatar_ybot::mixamorig_RightArm_y"]->SetPosition(0, mixamorig_RightArm_y);
        //   node.getParam("/RightForeArm", mixamorig_RightForeArm);
        //   m_joints["avatar_ybot::mixamorig_RightForeArm"]->SetPosition(0, mixamorig_RightForeArm);
        //   node.getParam("/RightHand_z", mixamorig_RightHand_z);
        //   m_joints["avatar_ybot::mixamorig_RightHand_z"]->SetPosition(0, mixamorig_RightHand_z);
        //   node.getParam("/RightHand_x", mixamorig_RightHand_x);
        //   m_joints["avatar_ybot::mixamorig_RightHand_x"]->SetPosition(0,mixamorig_RightHand_x);
        //   node.getParam("/RightHand_y", mixamorig_RightHand_y);
        //   m_joints["avatar_ybot::mixamorig_RightHand_y"]->SetPosition(0, mixamorig_RightHand_y);
        //   node.getParam("/RightHandThumb1_z", mixamorig_RightHandThumb1_z);
        //   m_joints["avatar_ybot::mixamorig_RightHandThumb1_z"]->SetPosition(0, mixamorig_RightHandThumb1_z);
      }
     
           


      // this->move_right_arm_to_target(false);
      
      // this->temporary_function_for_setting_zeros_other_joints();

      // bool finger_state;
      // node.getParam("/hand_close_avatar", finger_state);
      // if (finger_state){
      //    counter_f++;  
      //   this->finger_grasping_position(false);
      //   std::cout << "Finger closed" << std::endl;
      // }


      node.getParam("/mixamorig_RightArm_z", mixamorig_RightArm_z);
      node.getParam("/mixamorig_RightArm_x", mixamorig_RightArm_x);
      node.getParam("/mixamorig_RightArm_y", mixamorig_RightArm_y);
      node.getParam("/mixamorig_RightForeArm", mixamorig_RightForeArm);
      node.getParam("/mixamorig_RightHand_z", mixamorig_RightHand_z);
      node.getParam("/mixamorig_RightHand_x", mixamorig_RightHand_x);
      node.getParam("/mixamorig_RightHand_y", mixamorig_RightHand_y);

      bool counter_reset ; 
      node.getParam("/counter_reset", counter_reset);
      if (counter_reset)
      {
          counter_f = 0;
          counter_reset = false;
          node.setParam("/counter_reset", counter_reset);
          node.setParam("/trajectory_state_is_done", false);
          std::vector<double> empty_vector;
          node.setParam("/mixamorig_RightArm_z", empty_vector);
          node.setParam("/mixamorig_RightArm_x", empty_vector);
          node.setParam("/mixamorig_RightArm_y", empty_vector);
          node.setParam("/mixamorig_RightForeArm", empty_vector);
          node.setParam("/mixamorig_RightHand_z", empty_vector);
          node.setParam("/mixamorig_RightHand_x", empty_vector);
          node.setParam("/mixamorig_RightHand_y", empty_vector);
          
      }


  if (counter % 5 == 0 ) {
        counter_f++;
        if (mixamorig_RightArm_z.size() > counter_f){
        m_joints["avatar_ybot::mixamorig_RightArm_z"]->SetPosition(0, mixamorig_RightArm_z[counter_f]);
        m_joints["avatar_ybot::mixamorig_RightArm_x"]->SetPosition(0, mixamorig_RightArm_x[counter_f]);
        m_joints["avatar_ybot::mixamorig_RightArm_y"]->SetPosition(0, mixamorig_RightArm_y[counter_f]);
        m_joints["avatar_ybot::mixamorig_RightForeArm"]->SetPosition(0, mixamorig_RightForeArm[counter_f]);
        m_joints["avatar_ybot::mixamorig_RightHand_z"]->SetPosition(0, mixamorig_RightHand_z[counter_f]);
        m_joints["avatar_ybot::mixamorig_RightHand_x"]->SetPosition(0, mixamorig_RightHand_x[counter_f]);
        m_joints["avatar_ybot::mixamorig_RightHand_y"]->SetPosition(0, mixamorig_RightHand_y[counter_f]);
        }
        else if (mixamorig_RightArm_z.size() ==  counter_f){
            node.setParam("/trajectory_state_is_done", true);
        }


      }

      else {

            for (JointMap::iterator joint_iter = m_joints.begin(); joint_iter != m_joints.end(); ++joint_iter)
             {
            
              if ( m_joints[joint_iter->first]->DOF())
              {
                double joint_current_pos = m_joints[joint_iter->first]->Position(0);
                m_joints[joint_iter->first]->SetPosition(0,joint_current_pos);
              }

            }



          //  std::cout << "-------------------------" << std::endl;
          //  std::cout << m_joints["avatar_ybot::mixamorig_LeftArm_z"]->Position(0) << std::endl;
          //  std::cout << m_joints["avatar_ybot::mixamorig_LeftArm_x"]->Position(0) << std::endl;
          //  std::cout << m_joints["avatar_ybot::mixamorig_LeftArm_y"]->Position(0) << std::endl;
          //  std::cout << m_joints["avatar_ybot::mixamorig_LeftForeArm"]->Position(0) << std::endl;
          //  std::cout << m_joints["avatar_ybot::mixamorig_LeftHand_z"]->Position(0) << std::endl;
          //  std::cout << m_joints["avatar_ybot::mixamorig_LeftHand_x"]->Position(0) << std::endl;
          //  std::cout << m_joints["avatar_ybot::mixamorig_LeftHand_y"]->Position(0) << std::endl;
          //  std::cout << m_joints["avatar_ybot::mixamorig_LeftHandThumb1_z"]->Position(0) << std::endl;
          //  std::cout << "-------------------------" << std::endl;

      }

    // finger_grasping_position(false);

    }

      template <typename T>
      std::vector<T> AvatarModelGrasp::linspace(T a, T b, size_t N) {
      T h = (b - a) / static_cast<T>(N-1);
      std::vector<T> xs(N);
      typename std::vector<T>::iterator x;
      T val;
      for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h)
      *x = val;
      return xs;
      }


    void AvatarModelGrasp::move_right_arm_to_target(bool animation ){
        // This function will take the joint positin from the rosparameter server.

        std::vector<std::string> right_arm_joint_names {"mixamorig_RightArm_z","mixamorig_RightArm_x","mixamorig_RightArm_y",
                                  "mixamorig_RightForeArm","mixamorig_RightHand_z","mixamorig_RightHand_x","mixamorig_RightHand_y"};

        for (int i = 0 ; i<right_arm_joint_names.size(); i++){
            if (animation){
            this->joint_animation(m_joints["avatar_ybot::"+right_arm_joint_names[i]],steps,counter);

            }
            else{
            // double joint_pos;
            // node.getParam("/"+right_arm_joint_names[i], joint_pos);
            // m_joints["avatar_ybot::"+right_arm_joint_names[i]]->SetPosition(0,joint_pos); //direct movement
            // this->ControllerSpecificJoint("avatar_ybot::"+right_arm_joint_names[i],joint_pos,common::PID(1000, 100, 10));
            // std::cout << "Error: " << m_joints["avatar_ybot::"+right_arm_joint_names[i]]->GetName() << ": " << joint_pos - m_joints["avatar_ybot::"+right_arm_joint_names[i]]->Position(0) << std::endl; //error = target-current val

            std::vector<double> joint_pos;
            node.getParam("/"+right_arm_joint_names[i], joint_pos);
            for(int j =0 ; j<joint_pos.size() ; j++){
              m_joints["avatar_ybot::"+right_arm_joint_names[i]]->SetPosition(0,joint_pos[j]);
            }


            }

        }



    }

    void AvatarModelGrasp::finger_open(){

       m_joints["avatar_ybot::mixamorig_RightHandMiddle1_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandMiddle2_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandMiddle3_z"]->SetPosition(0, 0.1);
       m_joints["avatar_ybot::mixamorig_RightHandPinky1_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandPinky2_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandPinky3_z"]->SetPosition(0, 0.1);
       m_joints["avatar_ybot::mixamorig_RightHandRing1_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandRing2_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandRing3_z"]->SetPosition(0, 0.1);
       m_joints["avatar_ybot::mixamorig_RightHandThumb3"]->SetPosition(0, 0);


        m_joints["avatar_ybot::mixamorig_RightHandThumb1_z"]->SetPosition(0, 1);
       m_joints["avatar_ybot::mixamorig_RightHandThumb1_x"]->SetPosition(0, 0);
       m_joints["avatar_ybot::mixamorig_RightHandThumb1_y"]->SetPosition(0, -0.6);
       m_joints["avatar_ybot::mixamorig_RightHandThumb2"]->SetPosition(0, 0);

       m_joints["avatar_ybot::mixamorig_RightHandIndex1_z"]->SetPosition(0, 0);
       m_joints["avatar_ybot::mixamorig_RightHandIndex2_z"]->SetPosition(0, 0);
       m_joints["avatar_ybot::mixamorig_RightHandIndex3_z"]->SetPosition(0, 0);

      //  double f_pos;
      // node.getParam("/mixamorig_RightHandThumb1_z", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb1_z"]->SetPosition(0, f_pos);
      // node.getParam("/mixamorig_RightHandThumb1_x", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb1_x"]->SetPosition(0, f_pos);
      // node.getParam("/mixamorig_RightHandThumb1_y", f_pos); 
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb1_y"]->SetPosition(0, f_pos);
      //   node.getParam("/mixamorig_RightHandThumb2", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb2"]->SetPosition(0, f_pos);

      // node.getParam("/mixamorig_RightHandIndex1_z", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandIndex1_z"]->SetPosition(0, f_pos);
      //  node.getParam("/mixamorig_RightHandIndex2_z", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandIndex2_z"]->SetPosition(0, f_pos);
      //  node.getParam("/mixamorig_RightHandIndex3_z", f_pos);
      //  m_joints["avatar_ybot::mixamorig_RightHandIndex3_z"]->SetPosition(0, f_pos);




     
    }

    void AvatarModelGrasp::finger_close(){

       m_joints["avatar_ybot::mixamorig_RightHandMiddle1_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandMiddle2_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandMiddle3_z"]->SetPosition(0, 0.1);
       m_joints["avatar_ybot::mixamorig_RightHandPinky1_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandPinky2_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandPinky3_z"]->SetPosition(0, 0.1);
       m_joints["avatar_ybot::mixamorig_RightHandRing1_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandRing2_z"]->SetPosition(0, 1.57);
       m_joints["avatar_ybot::mixamorig_RightHandRing3_z"]->SetPosition(0, 0.1);
       

       m_joints["avatar_ybot::mixamorig_RightHandThumb1_z"]->SetPosition(0, 0.7);
       m_joints["avatar_ybot::mixamorig_RightHandThumb1_x"]->SetPosition(0, 0.7);
       m_joints["avatar_ybot::mixamorig_RightHandThumb1_y"]->SetPosition(0, -0.4);
       m_joints["avatar_ybot::mixamorig_RightHandThumb2"]->SetPosition(0, -0.8);
        m_joints["avatar_ybot::mixamorig_RightHandThumb3"]->SetPosition(0, 0.3);

       m_joints["avatar_ybot::mixamorig_RightHandIndex1_z"]->SetPosition(0, 0.2);
       m_joints["avatar_ybot::mixamorig_RightHandIndex2_z"]->SetPosition(0, 0.8);
       m_joints["avatar_ybot::mixamorig_RightHandIndex3_z"]->SetPosition(0, 0.6);

      //  double RightHandThumb1_z, RightHandThumb1_x,RightHandThumb1_y,RightHandThumb2,RightHandThumb3;
      //  node.getParam("/mixamorig_RightHandThumb1_z", RightHandThumb1_z);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb1_z"]->SetPosition(0, RightHandThumb1_z);
      //  node.getParam("/mixamorig_RightHandThumb1_x", RightHandThumb1_x);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb1_x"]->SetPosition(0, RightHandThumb1_x);
      //  node.getParam("/mixamorig_RightHandThumb1_y", RightHandThumb1_y);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb1_y"]->SetPosition(0, RightHandThumb1_y);
      //  node.getParam("/mixamorig_RightHandThumb2", RightHandThumb2);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb2"]->SetPosition(0, RightHandThumb2);
      //  node.getParam("/mixamorig_RightHandThumb3", RightHandThumb3);
      //  m_joints["avatar_ybot::mixamorig_RightHandThumb3"]->SetPosition(0, RightHandThumb3);
 


       double mixamorig_RightHandIndex1_z,mixamorig_RightHandIndex2_z,mixamorig_RightHandIndex3_z;
      node.getParam("/mixamorig_RightHandIndex1_z", mixamorig_RightHandIndex1_z);
       m_joints["avatar_ybot::mixamorig_RightHandIndex1_z"]->SetPosition(0, mixamorig_RightHandIndex1_z);
       node.getParam("/mixamorig_RightHandIndex2_z", mixamorig_RightHandIndex2_z);
       m_joints["avatar_ybot::mixamorig_RightHandIndex2_z"]->SetPosition(0, mixamorig_RightHandIndex2_z);
       node.getParam("/mixamorig_RightHandIndex3_z", mixamorig_RightHandIndex3_z);
       m_joints["avatar_ybot::mixamorig_RightHandIndex3_z"]->SetPosition(0, mixamorig_RightHandIndex3_z);




    }

    void AvatarModelGrasp::finger_grasping_position(bool animation ){

      std::vector<std::string> right_hand_fingers {"mixamorig_RightHandMiddle1_z","mixamorig_RightHandMiddle2_z","mixamorig_RightHandMiddle3_z",
                                  "mixamorig_RightHandPinky1_z","mixamorig_RightHandPinky2_z","mixamorig_RightHandPinky3_z",
                                  "mixamorig_RightHandRing1_z", "mixamorig_RightHandRing2_z","mixamorig_RightHandRing3_z",
                                  "mixamorig_RightHandThumb1_y","mixamorig_RightHandThumb2","mixamorig_RightHandThumb3"};
      for (int i = 0 ; i<right_hand_fingers.size(); i++){
            if (animation){
            this->joint_animation(m_joints["avatar_ybot::"+right_hand_fingers[i]],steps,counter_f);

            }
            else{
            double joint_pos;
            node.getParam("/"+right_hand_fingers[i], joint_pos);
            // m_joints["avatar_ybot::"+right_hand_fingers[i]]->SetPosition(0,joint_pos); //direct movement
            // this->ControllerSpecificJoint("avatar_ybot::"+right_hand_fingers[i],joint_pos,common::PID(1000, 100, 10));

              // std::cout << right_hand_fingers[i] << m_joints["avatar_ybot::"+right_hand_fingers[i]]->Position(0) << std::endl;

            }

        }                                  





    }

 
    void AvatarModelGrasp::joint_animation(gazebo::physics::JointPtr &joint, int steps, int counter){

        double pos_val; // position value from the ros param server
        node.getParam("/" + joint->GetName(), pos_val);
        if (std::abs(pos_val) > std::abs(pos_val / double(steps) * counter))
        {

            joint->SetPosition(0, pos_val / double(steps) * counter);
        }
    }

    void AvatarModelGrasp::SimulationDelay(double second){
      // Get current time and delay the simulation.
      auto end = std::chrono::system_clock::now();
      std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    //   std::cout << "Current Time Now:  " << std::ctime(&end_time);
      usleep(second * 1000000 ); // 1 second = 1e6 micro seconds

    }

    void AvatarModelGrasp::ControllerSpecificJoint(std::string joint_name, double target_pos,common::PID pid){
    //   m_joint_controller =  model->GetJointController();
      m_joint_controller->SetPositionPID(joint_name, pid);
      m_joint_controller->SetPositionTarget(joint_name, target_pos);


    }

    void AvatarModelGrasp::GetAllJoints(){
        ROS_WARN("GetAllJoints");
        m_joint_controller = this->model->GetJointController();
        if (m_joint_controller){// Null if there are no joints
            m_joints = m_joint_controller->GetJoints(); 
        }
        ROS_WARN("Number of joints...%ld", m_joints.size());
           
    }

    void AvatarModelGrasp::temporary_function_for_setting_zeros_other_joints(){
      m_joints["avatar_ybot::mixamorig_Spine"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_Spine1"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_Spine2"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_Neck"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_Head"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_HeadTop_End"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftEye"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_RightEye"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftShoulder"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftArm_z"]->SetPosition(0, -0.502202);
      m_joints["avatar_ybot::mixamorig_LeftArm_x"]->SetPosition(0, 0.734888);
      m_joints["avatar_ybot::mixamorig_LeftArm_y"]->SetPosition(0, -1.93325);
      m_joints["avatar_ybot::mixamorig_LeftForeArm"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHand_z"]->SetPosition(0, -2.09809);
      m_joints["avatar_ybot::mixamorig_LeftHand_x"]->SetPosition(0, 0.782512);
      m_joints["avatar_ybot::mixamorig_LeftHand_y"]->SetPosition(0, -0.380571);
      m_joints["avatar_ybot::mixamorig_LeftHandMiddle1"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandMiddle2"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandMiddle3"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandMiddle4"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandThumb1_z"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandThumb1_x"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandThumb1_y"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandThumb2"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandThumb3"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandThumb4"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandIndex1"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandIndex2"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandIndex3"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandIndex4"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandRing1"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandRing2"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandRing3"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandRing4"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandPinky1"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandPinky2"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandPinky3"]->SetPosition(0, 0);
      m_joints["avatar_ybot::mixamorig_LeftHandPinky4"]->SetPosition(0, 0);

    }

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(AvatarModelGrasp)