#include <stdio.h>
#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainiksolvervel_pinv_givens.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <Eigen/Dense>
using namespace std;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "test_inertia");
    KDL::Tree my_tree;
    ros::NodeHandle node;
    string robot_desc_string;
    node.param("robot_description", robot_desc_string, string());
    if (!kdl_parser::treeFromString(robot_desc_string, my_tree))
    {
        ROS_ERROR("Failed to construct kdl tree");
        return false;
    }
    KDL::Chain kuka_arm_chain;
    my_tree.getChain("mixamorig_Hips", "mixamorig_LeftHand", kuka_arm_chain);

    int number_of_joints = kuka_arm_chain.getNrOfJoints();
    ROS_WARN("Number of joints: %d\n", number_of_joints);

    // Transformation target based on gazebo frame with kuka robot
    KDL::Vector pos_hips_to_world = KDL::Vector(-0.575,0.1,0); //dont change
    KDL::Rotation rot_hips_to_world;
    rot_hips_to_world = rot_hips_to_world.Quaternion(0,0,0.7071068, 0.7071068);
    // rot_target = rot_target.Quaternion(0,0,0,1);
    KDL::Frame T_hips_to_world(rot_hips_to_world, pos_hips_to_world);







    // Inverse kinematic solution test

    double eps = 1E-5;
    int maxiter = 1500;
    double eps_joints = 1E-15;
    KDL::ChainIkSolverPos_LMA iksolver = KDL::ChainIkSolverPos_LMA(kuka_arm_chain, eps, maxiter, eps_joints);

        // This is global coordinate target frame , change this to give target pos 
    // KDL::Vector pos_target = KDL::Vector(-0.15,-0.14,1.17);
    KDL::Vector pos_target = KDL::Vector(-0.15,-0.14,1.17);
    KDL::Rotation rot_target;
    // rot_target = rot_target.Quaternion(0.7071068, 0, 0, 0.7071068) * rot_target.Quaternion(0, 0.7071068, 0, 0.7071068);
    rot_target = rot_target.Quaternion(0,0,1,0);
    KDL::Frame T_goal(rot_target, pos_target);

    T_goal = T_hips_to_world.Inverse() * T_goal;

    Eigen::MatrixXd Goal_Frame_Matrix(4, 4);
    Goal_Frame_Matrix << T_goal(0, 0), T_goal(0, 1), T_goal(0, 2), T_goal(0, 3),
        T_goal(1, 0), T_goal(1, 1), T_goal(1, 2), T_goal(1, 3),
        T_goal(2, 0), T_goal(2, 1), T_goal(2, 2), T_goal(2, 3),
        T_goal(3, 0), T_goal(3, 1), T_goal(3, 2), T_goal(3, 3);

    std::cout << "Goal Transformation Matrix: " << std::endl;
    std::cout << Goal_Frame_Matrix << std::endl;

    // The model should calculate inverse kinematics based on the following frame.

    KDL::JntArray jointpositions = KDL::JntArray(number_of_joints);
    KDL::JntArray jointGuesspositions = KDL::JntArray(number_of_joints);

    bool kinematics_status;
    KDL::JntArray q(number_of_joints); // used for calculation forward kinematics
    kinematics_status = iksolver.CartToJnt(jointGuesspositions, T_goal, jointpositions);
    if (kinematics_status >= 0)
    {
        for (int i = 0; i < number_of_joints; i++)
        {
            std::cout << jointpositions(i) << std::endl;
            q(i) = jointpositions(i);
        }
        printf("%s \n", "Success, thanks KDL!");
    }
    else
    {
        printf("%s \n", "Error:could not calculate backword kinematics : ");
    }

    node.setParam("/mixamorig_RightArm_z", jointpositions(0));
    node.setParam("/mixamorig_RightArm_x", jointpositions(1));
    node.setParam("/mixamorig_RightArm_y", jointpositions(2));
    node.setParam("/mixamorig_RightForeArm", jointpositions(3));
    node.setParam("/mixamorig_RightHand_z", jointpositions(4));
    node.setParam("/mixamorig_RightHand_x", jointpositions(5));
    node.setParam("/mixamorig_RightHand_y", jointpositions(6));
    // node.setParam("/mixamorig_RightHandIndex1",jointpositions(7));
    // node.setParam("/mixamorig_RightHandIndex1_z",jointpositions(8));
    // node.setParam("/mixamorig_RightHandIndex2",jointpositions(9));
    // node.setParam("/mixamorig_RightHandIndex2_z",jointpositions(10));
    // node.setParam("/mixamorig_RightHandIndex3",jointpositions(11));
    // node.setParam("/mixamorig_RightHandIndex3_z",jointpositions(12));
    // node.setParam("/mixamorig_RightHandIndex4",jointpositions(13));

    node.setParam("/grasp_hanoi_token", false);


    node.setParam("/hand_close_avatar", false);

    node.setParam("/mixamorig_RightHandIndex1_z", 0.2169);
    node.setParam("/mixamorig_RightHandIndex2_z", 1.5707);
    node.setParam("/mixamorig_RightHandIndex3_z", 0);

    node.setParam("/mixamorig_RightHandMiddle1_z", 1.17);
    node.setParam("/mixamorig_RightHandMiddle2_z", 1.17);
    node.setParam("/mixamorig_RightHandMiddle3_z", 1.17);

    node.setParam("/mixamorig_RightHandRing1_z", 1.17);
    node.setParam("/mixamorig_RightHandRing2_z", 1.17);
    node.setParam("/mixamorig_RightHandRing3_z", 1.17);

    node.setParam("/mixamorig_RightHandPinky1_z", 1.17);
    node.setParam("/mixamorig_RightHandPinky2_z", 1.17);
    node.setParam("/mixamorig_RightHandPinky3_z", 1.17);

    node.setParam("/mixamorig_RightHandThumb1_y",0.56);
    node.setParam("/mixamorig_RightHandThumb2", -0.4426);
    node.setParam("/mixamorig_RightHandThumb3", 0);



    std::cout << "Verify the solution with Forward Kinematics" << std::endl;

    KDL::ChainFkSolverPos_recursive fksolver(kuka_arm_chain);
    KDL::Frame T;
    fksolver.JntToCart(q, T);

    std::cout << "" << std::endl;
    std::cout << "Forward Kinematic solution Matrix: " << std::endl;

    Eigen::MatrixXd Frame_Matrix(4, 4);
    Frame_Matrix << T(0, 0), T(0, 1), T(0, 2), T(0, 3),
        T(1, 0), T(1, 1), T(1, 2), T(1, 3),
        T(2, 0), T(2, 1), T(2, 2), T(2, 3),
        T(3, 0), T(3, 1), T(3, 2), T(3, 3);

    std::cout << Frame_Matrix << std::endl;

    Eigen::Quaterniond forward_quaternion(Frame_Matrix.block<3, 3>(0, 0));
    std::cout << "Forward Quaternion" << std::endl;
    std::cout << "x: " << forward_quaternion.x() << " y: " << forward_quaternion.y() << " z: " << forward_quaternion.z() << " w: " << forward_quaternion.w() << std::endl;




        node.setParam("/mixamorig_LeftArm_z", -0.65);
        node.setParam("/mixamorig_LeftArm_x", 0.1); 
        node.setParam("/mixamorig_LeftArm_y", -0.8);
        node.setParam("/mixamorig_LeftForeArm", 0);
        node.setParam("/mixamorig_LeftHand_z", -0.11);
        node.setParam("/mixamorig_LeftHand_x", 0.6);
        node.setParam("/mixamorig_LeftHand_y", -0.5);


}
