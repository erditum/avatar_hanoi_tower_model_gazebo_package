
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>
#include <stdio.h>
#include <ros/ros.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainiksolvervel_pinv_givens.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <Eigen/Dense>


class For_and_Inv_Kinematic_Solver{

  private:
  KDL::Tree my_tree;
  std::string robot_desc_string;
  ros::NodeHandle node;
  KDL::Chain kuka_arm_chain;
  
  
  
  public:

  For_and_Inv_Kinematic_Solver(){

      node.param("robot_description", robot_desc_string, std::string());
      
      if (!kdl_parser::treeFromString(robot_desc_string, my_tree))
      {
          ROS_ERROR("Failed to construct kdl tree");
          
      }

      my_tree.getChain("mixamorig_Hips", "mixamorig_RightHand", kuka_arm_chain);
    
  }


  int get_number_of_joints(){
    int number_of_joints = kuka_arm_chain.getNrOfJoints();
    return number_of_joints; 

  }

  void linspace(KDL::Vector vec1, KDL::Vector vec2, size_t N , std::vector<KDL::Vector> &trajectory) {

   

    double h_0 = (vec2[0] - vec1[0]) / static_cast<double>(N-1);
    double h_1 = (vec2[1] - vec1[1]) / static_cast<double>(N-1);
    double h_2 = (vec2[2] - vec1[2]) / static_cast<double>(N-1);
    KDL::Vector xs;
    for (int i = 0 ; i<N ; i++){
        xs[0] = vec1[0] + h_0 * i ; 
        xs[1] = vec1[1] + h_1 * i ; 
        xs[2] = vec1[2] + h_2 * i ; 
        trajectory.push_back(xs);
    }
  }

  void SimulationDelay(double second){
      // Get current time and delay the simulation.
      auto end = std::chrono::system_clock::now();
      std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    //   std::cout << "Current Time Now:  " << std::ctime(&end_time);
      usleep(second * 1000000 ); // 1 second = 1e6 micro seconds

    }

    KDL::Frame Transformation_matrix_hips_to_world_frame(){
    // This transformation matrix based on the location hips to the world frame
    KDL::Vector pos_hips_to_world = KDL::Vector(-0.45832319676974104, 0.09999866419115455,0); //dont change
    KDL::Rotation rot_hips_to_world;
    rot_hips_to_world = rot_hips_to_world.Quaternion(0,0,0.7071068, 0.7071068);
    // rot_target = rot_target.Quaternion(0,0,0,1);
    KDL::Frame T_hips_to_world(rot_hips_to_world, pos_hips_to_world);

    return T_hips_to_world;

    }

    KDL::Frame go_target_position(KDL::Vector pos, KDL::Rotation rot){
    

    KDL::Frame T_goal(rot, pos);
    T_goal = Transformation_matrix_hips_to_world_frame().Inverse() * T_goal;

    return T_goal;

}



KDL::JntArray  inverse_kinematic_solver(KDL::Frame T_goal){

    // Takes goal frame return the joint angles

    // Inverse kinematic solution test
    double eps = 1E-5;
    int maxiter = 1500;
    double eps_joints = 1E-15;
    KDL::ChainIkSolverPos_LMA iksolver = KDL::ChainIkSolverPos_LMA(kuka_arm_chain, eps, maxiter, eps_joints);
    
    int number_of_joints = get_number_of_joints();

    KDL::JntArray jointpositions = KDL::JntArray(number_of_joints);
    KDL::JntArray jointGuesspositions = KDL::JntArray(number_of_joints);

    bool kinematics_status;
    KDL::JntArray q(number_of_joints); // used for calculation forward kinematics
    kinematics_status = iksolver.CartToJnt(jointGuesspositions, T_goal, jointpositions);
    if (kinematics_status >= 0)
    {
        for (int i = 0; i < number_of_joints; i++)
        {
            std::cout << jointpositions(i) << std::endl;
            q(i) = jointpositions(i);
        }
        printf("%s \n", "Success, thanks KDL!");
    }
    else
    {
        printf("%s \n", "Error:could not calculate backword kinematics : ");
    }
    return jointpositions;
}






};

int main(int argc, char** argv){
  ros::init(argc, argv, "avatar_trajectory_follower_and_tf_listener_combined");

  ros::NodeHandle node;

  For_and_Inv_Kinematic_Solver test_class;
  tf::TransformListener listener;

  ros::Rate rate(10.0);
  while (node.ok()){
    tf::StampedTransform transform;
    try{
        ros::Time now = ros::Time::now();
        listener.waitForTransform("/world", "/hanoi_tower_d300_frame", now, ros::Duration(3.0));
        listener.lookupTransform("/world","/hanoi_tower_d300_frame",ros::Time(0), transform);
        std::cout << "transform exist\n";

        std::cout << "X: " << transform.getOrigin().x() << std::endl;
        std::cout << "Y: " << transform.getOrigin().y() << std::endl;
        std::cout << "Z: " << transform.getOrigin().z() << std::endl;

        std::cout << "rotx : " << transform.getRotation().x() << std::endl;
        std::cout << "roty : " << transform.getRotation().y() << std::endl;
        std::cout << "rotz : " << transform.getRotation().z() << std::endl;
        std::cout << "rotw : " << transform.getRotation().w() << std::endl;

          KDL::Rotation rot_target;
          rot_target = rot_target.Quaternion(0,0,1,0);
          KDL::Frame T_goal = test_class.go_target_position(KDL::Vector(0.1,-0.24-0.005,1.17),rot_target);
          test_class.inverse_kinematic_solver(T_goal);
 
    }
    catch (tf::TransformException &ex) {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
      continue;
    }

    rate.sleep();
  }
  return 0;
};