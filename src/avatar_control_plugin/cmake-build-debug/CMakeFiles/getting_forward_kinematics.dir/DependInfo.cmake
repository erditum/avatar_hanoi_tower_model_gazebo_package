# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/erdi/Documents/NRP/GazeboRosPackages/src/avatar_control_plugin/src/getting_forward_kinematics.cpp" "/home/erdi/Documents/NRP/GazeboRosPackages/src/avatar_control_plugin/cmake-build-debug/CMakeFiles/getting_forward_kinematics.dir/src/getting_forward_kinematics.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_TEST_DYN_LINK"
  "LIBBULLET_VERSION=2.88"
  "LIBBULLET_VERSION_GT_282"
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"avatar_control_plugin\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/home/erdi/Documents/NRP/GazeboRosPackages/devel/include"
  "/opt/ros/noetic/include"
  "/opt/ros/noetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/usr/include/eigen3"
  "/home/erdi/.local/include"
  "/home/erdi/.local/include/gazebo-11"
  "/usr/include/bullet"
  "/home/erdi/.local/include/simbody"
  "/home/erdi/.local/include/sdformat-9.2"
  "/usr/include/ignition/math6"
  "/usr/include/OGRE"
  "/usr/include/OGRE/Terrain"
  "/usr/include/OGRE/Paging"
  "/usr/include/ignition/transport8"
  "/usr/include/ignition/msgs5"
  "/usr/include/ignition/common3"
  "/usr/include/ignition/fuel_tools4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
