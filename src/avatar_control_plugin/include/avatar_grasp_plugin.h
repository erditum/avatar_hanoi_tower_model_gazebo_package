#ifndef AVATAR_GRASP_PLUGIN_H
#define AVATAR_GRASP_PLUGIN_H
#include <ros/ros.h> 
using namespace gazebo;

class AvatarModelGrasp : public ModelPlugin{
    public:
        //Constructors
        AvatarModelGrasp();
        // Destructors
        // ~AvatarModelGrasp();
        // Load the plugin and initilize all controllers
        void Load(physics::ModelPtr parent, sdf::ElementPtr sdf);
        // Simulation update callback function
        void OnUpdate();

        void SimulationDelay(double second);

        void GetAllJoints();

        void ControllerSpecificJoint(std::string joint_name, double target_pos,common::PID pid);

        void temporary_function_for_setting_zeros_other_joints();

        void move_right_arm_to_target(bool animation);


        void finger_grasping_position(bool animation);

        void finger_open();

        void finger_close();

        
        

        void joint_animation(gazebo::physics::JointPtr &joint, int steps, int counter);


        template <typename T>
        std::vector<T> linspace(T a, T b, size_t N);


    private: 
        // Pointer to the model
        physics::ModelPtr model;
        // Pointer to the update event connection
        event::ConnectionPtr updateConnection;

        //Ros handle
        ros::NodeHandle node;

        int counter = 0;
        int counter_f = 0;
        int steps = 1000;
        int counter_right_hand_home =0;

        // Joint controller pointer
        physics::JointControllerPtr m_joint_controller;

        typedef std::map<std::string, physics::JointPtr> JointMap;
        // Map of joint pointers
        JointMap m_joints;
        std::vector<double> mixamorig_RightArm_z;
      std::vector<double> mixamorig_RightArm_x;
      std::vector<double> mixamorig_RightArm_y;
      std::vector<double> mixamorig_RightForeArm;
      std::vector<double> mixamorig_RightHand_z;
      std::vector<double> mixamorig_RightHand_x;
      std::vector<double> mixamorig_RightHand_y;



};




#endif