#!/usr/bin/env bash

# roscmd arg1 [arg2] arg_id_to_complete

# es. rostopic ec 1 -> echo
# rostopic echo 2 -> list of topics
if [[ $# -lt 2 ]]; then
    printf "ERROR! At least 2 parameters expected.\nUSAGE: <roscmd> [args] <arg_id_to_complete>"
    exit 2
fi

args=( "${@}" )

# check that <arg_id_to_complete> must is a number
export COMP_CWORD=${args[@]: -1} # last arg.

if ! [[ $COMP_CWORD =~ ^[0-9]+$ ]] ; then
   echo "ERROR!: last argument <arg_id_to_complete> must be a number" >&2
   exit 1
fi

# remove last element from $@
# es. (rostopic echo 2) -> (rostopic echo)
export COMP_WORDS=( "${args[@]:0: ${#args[@]} -1}" )

rosbash_path="/opt/ros/$ROS_DISTRO/share/rosbash/rosbash"

# load ros completion functions
if [ -e "$rosbash_path" ]; then
  source "$rosbash_path"
else
  (echo >&2 "ROS not found!")
  exit 1
fi

# ${COMP_WORDS[0]} is the ros command (eg 'rosmsg', 'rostopic')
# so we'll evaluate_{roscmd} (eg. roscomplete_rosmsg, roscomplete_rostopic)
eval _roscomplete_"${COMP_WORDS[0]}"
printf '%s\n' "${COMPREPLY[@]}"
