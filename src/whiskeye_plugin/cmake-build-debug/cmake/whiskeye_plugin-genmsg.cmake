# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "whiskeye_plugin: 1 messages, 0 services")

set(MSG_I_FLAGS "-Iwhiskeye_plugin:/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(whiskeye_plugin_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg" NAME_WE)
add_custom_target(_whiskeye_plugin_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "whiskeye_plugin" "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg" "std_msgs/Header:std_msgs/Float32MultiArray:std_msgs/MultiArrayDimension:std_msgs/MultiArrayLayout:std_msgs/Bool"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(whiskeye_plugin
  "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Float32MultiArray.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/MultiArrayDimension.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/MultiArrayLayout.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Bool.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/whiskeye_plugin
)

### Generating Services

### Generating Module File
_generate_module_cpp(whiskeye_plugin
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/whiskeye_plugin
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(whiskeye_plugin_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(whiskeye_plugin_generate_messages whiskeye_plugin_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg" NAME_WE)
add_dependencies(whiskeye_plugin_generate_messages_cpp _whiskeye_plugin_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(whiskeye_plugin_gencpp)
add_dependencies(whiskeye_plugin_gencpp whiskeye_plugin_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS whiskeye_plugin_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(whiskeye_plugin
  "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Float32MultiArray.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/MultiArrayDimension.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/MultiArrayLayout.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Bool.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/whiskeye_plugin
)

### Generating Services

### Generating Module File
_generate_module_eus(whiskeye_plugin
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/whiskeye_plugin
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(whiskeye_plugin_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(whiskeye_plugin_generate_messages whiskeye_plugin_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg" NAME_WE)
add_dependencies(whiskeye_plugin_generate_messages_eus _whiskeye_plugin_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(whiskeye_plugin_geneus)
add_dependencies(whiskeye_plugin_geneus whiskeye_plugin_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS whiskeye_plugin_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(whiskeye_plugin
  "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Float32MultiArray.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/MultiArrayDimension.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/MultiArrayLayout.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Bool.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/whiskeye_plugin
)

### Generating Services

### Generating Module File
_generate_module_lisp(whiskeye_plugin
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/whiskeye_plugin
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(whiskeye_plugin_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(whiskeye_plugin_generate_messages whiskeye_plugin_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg" NAME_WE)
add_dependencies(whiskeye_plugin_generate_messages_lisp _whiskeye_plugin_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(whiskeye_plugin_genlisp)
add_dependencies(whiskeye_plugin_genlisp whiskeye_plugin_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS whiskeye_plugin_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(whiskeye_plugin
  "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Float32MultiArray.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/MultiArrayDimension.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/MultiArrayLayout.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Bool.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/whiskeye_plugin
)

### Generating Services

### Generating Module File
_generate_module_nodejs(whiskeye_plugin
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/whiskeye_plugin
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(whiskeye_plugin_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(whiskeye_plugin_generate_messages whiskeye_plugin_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg" NAME_WE)
add_dependencies(whiskeye_plugin_generate_messages_nodejs _whiskeye_plugin_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(whiskeye_plugin_gennodejs)
add_dependencies(whiskeye_plugin_gennodejs whiskeye_plugin_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS whiskeye_plugin_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(whiskeye_plugin
  "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Float32MultiArray.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/MultiArrayDimension.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/MultiArrayLayout.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Bool.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/whiskeye_plugin
)

### Generating Services

### Generating Module File
_generate_module_py(whiskeye_plugin
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/whiskeye_plugin
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(whiskeye_plugin_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(whiskeye_plugin_generate_messages whiskeye_plugin_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/erdi/Documents/NRP/GazeboRosPackages/src/whiskeye_plugin/msg/Bridge_u.msg" NAME_WE)
add_dependencies(whiskeye_plugin_generate_messages_py _whiskeye_plugin_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(whiskeye_plugin_genpy)
add_dependencies(whiskeye_plugin_genpy whiskeye_plugin_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS whiskeye_plugin_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/whiskeye_plugin)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/whiskeye_plugin
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(whiskeye_plugin_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/whiskeye_plugin)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/whiskeye_plugin
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(whiskeye_plugin_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/whiskeye_plugin)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/whiskeye_plugin
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(whiskeye_plugin_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/whiskeye_plugin)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/whiskeye_plugin
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(whiskeye_plugin_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/whiskeye_plugin)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/whiskeye_plugin\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/whiskeye_plugin
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(whiskeye_plugin_generate_messages_py std_msgs_generate_messages_py)
endif()
