#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>

namespace gazebo
{
  class ModelPush : public ModelPlugin
  {
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
      // Store the pointer to the model

      this->model = _parent;
      std::cout << "Model is loaded" << std::endl;
      std::cout << "Models are loaded" << std::endl;

      // Listen to the update event. This event is broadcast every
      // simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&ModelPush::OnUpdate, this));
    }

  

    // Pointer to the model
    private: physics::ModelPtr model;

    // Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;


      // Called by the world update start event
    public: void OnUpdate()
    {
      // Apply a small linear velocity to the model.
    //   this->model->SetLinearVel(ignition::math::Vector3d(.3, 0, 0));
      std::cout << this->model->GetName() << std::endl;
      this->model->SetLinearVel(ignition::math::Vector3d(.3, 0, 0));
      this->model->GetJoint("iiwa_joint_1");

      std::cout << this->model->GetJoint("iiwa_joint_1") << std::endl;
      

    }

    
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ModelPush)
}