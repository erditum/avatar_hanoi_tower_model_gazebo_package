#!/usr/bin/python
import rospy
from std_msgs.msg import String
import geometry_msgs.msg
from gazebo_msgs.msg import ModelStates
import tf2_ros

import numpy as np
from math import pi
#  This matchers is used to find how many ea_robots there are in the Gazebo
obj_starts_with = "hanoi_tower"
hanoi_board_pilar_height = 0.105
hanoi_board_pilar_distance = 0.1 * 1.2
gripper_length = 0.1
def shutdownhook():
    print('System is being closed')

def create_broadcaster(frame_id,child_frame_id,translation,rotation):
    """ create broadcast messages and publish
        :param frame_id: (str) name of the frame_id
        :param child_frame_id: (str) name of the child_frame_id
        :param translation: (list) [x,y,z]
        :param rotation: (list) [x,y,z,w]
    """
    br = tf2_ros.TransformBroadcaster()
    msg = geometry_msgs.msg.TransformStamped()
    msg.header.stamp = rospy.Time.now()
    msg.header.frame_id = frame_id
    msg.child_frame_id = child_frame_id
    msg.transform.translation.x = translation[0]
    msg.transform.translation.y = translation[1]
    msg.transform.translation.z = translation[2]
    msg.transform.rotation.x = rotation[0]
    msg.transform.rotation.y = rotation[1]
    msg.transform.rotation.z = rotation[2]
    msg.transform.rotation.w = rotation[3]
    br.sendTransform(msg)

def callback(data):
    object_list_in_gazebo = data.name #All available objects name in Gazebo
    all_objects_startswith = [i for i in data.name if i.startswith(obj_starts_with)] # All objects in Gazebo starts with specified string
    index_number = object_list_in_gazebo.index('hanoi_board')
    trans_board = data.pose[index_number].position
    orien_board = data.pose[index_number].orientation

    #Get the robot position with respect to gazebo
    index_number_robot = object_list_in_gazebo.index('iiwa14')
    trans_robot = data.pose[index_number_robot].position
    orien_robot = data.pose[index_number_robot].orientation


    #Broadcasting Gazebo Reference Frame based on the robot position.

    create_broadcaster(frame_id="world",
                       child_frame_id="gazebo_reference",
                       translation=[-trans_robot.x,-trans_robot.y,-trans_robot.z],
                       rotation=[0,0,0,1])

    create_broadcaster(frame_id="gazebo_reference",
                       child_frame_id="hanoi_board_middle_below_reference_frame",
                       translation=[trans_board.x,trans_board.y,trans_board.z],
                       rotation=[orien_board.x,orien_board.y,orien_board.z,orien_board.w])
    
    create_broadcaster(frame_id="gazebo_reference",
                       child_frame_id="hanoi_board_middle_top_reference_frame",
                       translation=[trans_board.x,trans_board.y,trans_board.z+hanoi_board_pilar_height],
                       rotation=[orien_board.x,orien_board.y,orien_board.z,orien_board.w])

    create_broadcaster(frame_id="gazebo_reference",
                       child_frame_id="hanoi_board_left_top_reference_frame",
                       translation=[trans_board.x,trans_board.y-hanoi_board_pilar_distance,trans_board.z+hanoi_board_pilar_height],
                       rotation=[orien_board.x,orien_board.y,orien_board.z,orien_board.w])                                             

    create_broadcaster(frame_id="gazebo_reference",
                       child_frame_id="hanoi_board_right_top_reference_frame",
                       translation=[trans_board.x,trans_board.y+hanoi_board_pilar_distance,trans_board.z+hanoi_board_pilar_height],
                       rotation=[orien_board.x,orien_board.y,orien_board.z,orien_board.w])

    create_broadcaster(frame_id="iiwa_link_ee",
                       child_frame_id="end_effector_frame",
                       translation=[0,0,gripper_length],
                       rotation=[0,0,0,1])
    for each_obj in all_objects_startswith:
        index_number = object_list_in_gazebo.index(each_obj)
        trans = data.pose[index_number].position
        orien = data.pose[index_number].orientation
        create_broadcaster(frame_id="gazebo_reference",
                           child_frame_id=each_obj+str('_frame'),
                           translation=[trans.x, trans.y, trans.z],
                           rotation=[orien.x, orien.y, orien.z, orien.w])

if __name__ == "__main__":
    rospy.init_node('hanoi_tower_reference_frame_broadcaster')
    rospy.logwarn("ROS TF Broadcaster is initialized...")
    rospy.Subscriber("/gazebo/model_states", ModelStates, callback)
    rospy.Rate(10)
    rospy.on_shutdown(shutdownhook)
    rospy.spin()
